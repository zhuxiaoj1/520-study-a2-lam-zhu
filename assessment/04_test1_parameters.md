---
title:  'Test 1 - Parameters'
author:
- Jaya Nilakantan
- 420-520-DW
---

# Parameters of Test 1

* the test will take place in the lab, at the start of the lab
* the test will be held on the lab computers (no laptops or other electronics allowed)
* the test questions will be on Moodle
  * theory-type questions are answered directly in Moodle (e.g., multiple choice, fill-in-the-blank, matching). Students will be allowed to go back and look over their answers. However, unlike the reading quizzes, only 1 attempt will be allowed
  * there will be a coding question; students will upload their `js` files to Moodle
* The test is closed-book: students may **not** consult their H: drive, notes, exercises, or web sites (e.g., GitLab, Stack Overflow, Google Search), etc...
* students **are** allowed to use the following tools:
  * Visual Studio Code, including LiveServer and eslint extensions (eslint will work since you will be coding on the desktop)
  * Google Chrome or Firefox Web Browser
  * the browser's development tools, including console
* All students will be asked to reboot their lab computers at the start of the test. 
* Disclosure: Your instructor will use a software available in the labs to monitor applications and record all use in case of dispute.

Last note: We are all trying to get used to in-person teaching and evaluations, so I don't promise that everything will run smoothly. The test (long quiz?) will be timed to be completable in around 45 minutes, but you will get 75 minutes. Once everyone is done, you can use the rest of the lab time to work on Assignment 2.