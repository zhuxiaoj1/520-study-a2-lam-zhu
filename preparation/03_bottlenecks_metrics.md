---
title:  'Week 3 Preparation: Common Performance Bottlenecks, Async Function Scope'
author:
- Maja Frydrychowicz, Jaya Nilakantan
- 420-520-DW
---

# Introduction

__Look at the material below with the goal of broad understanding__. You don't 
have to understand or remember all the details, just the big ideas.

* Write down questions that come to mind.
* Take high-level notes.

# Concepts

These are the keywords you should pay most attention to:

* render-blocking resources
* critical resources
* media query
* flash of unstyled content
* network roundtrip
* image optimization
* minification
* compression
* user experience
* user bounce rate
* idle time / idle work
* top-level await
* nested scope

# Tasks

Read the following:

* [Evaluate User Experience with RAIL (Response-Animation-Idle-Load)](https://www.keycdn.com/blog/rail-performance-model)
* Common Performance Bottlenecks
  * [Simple Case Study of Critical Rendering Path](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/analyzing-crp#performance_patterns)
  * [Not all CSS has to be render-blocking](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/render-blocking-css)
  * [Easiest performance wins](https://raygun.com/blog/resolve-frontend-performance-bottlenecks/#performance-tuning)
* Using DocumentFragments to reduce reflow  
  * [Appending multiple children with a DocumentFragment](https://blog.konnor.site/javascript/documentFragments/)
* Working with Promises and `async`
  * [Passing data between callback](https://2ality.com/2017/08/promise-callback-data-flow.html)
  * [Immediately invoked async arrow functions](https://exploringjs.com/impatient-js/ch_async-functions.html#immediately-invoked-async-arrow-functions)

