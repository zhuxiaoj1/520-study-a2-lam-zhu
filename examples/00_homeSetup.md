---
title:  'Getting set up at home or on your laptop'
author:
- Maja Frydrychowicz, Jaya Nilakantan
- 420-520-DW
---

Here are the tools that you will need to install at home to match the set up that we have in the labs:

* Set up VSCode
  * if you don't have it, download the most recent version of Visual Studio Code https://code.visualstudio.com/download 
  * Check Auto Save (File menu)
  * Install support for JS
  * add extension `eslint`
  * add extension `LiveServer`
  * add extension `npm`
  * add extension `Express`
  * add extension `MongoDB`
* install gitbash: https://git-scm.com/download/win 
* install Node.js: https://nodejs.org/en/download/
* Once node is installed, the rest is installed using the Node Package Manager npm through gitbash:
  * `npm install express-generator -g`
  * `npm install eslint -g`
  * `npm install -g minifier html-minify`
* MongoDB Compass: https://www.mongodb.com/try/download/compass 

